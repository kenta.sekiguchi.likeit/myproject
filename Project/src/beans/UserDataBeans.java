package beans;

import java.io.Serializable;

public class UserDataBeans implements Serializable {
	private String nname;
	private String mail;
	private String password;
	private String name;
	private String kananame;
	private String date;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getKananame() {
		return kananame;
	}
	public void setKananame(String kananame) {
		this.kananame = kananame;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getNname() {
		return nname;
	}
	public void setNname(String nname) {
		this.nname = nname;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}

}
