package beans;

import java.io.Serializable;
import java.util.Date;

public class BuyDataBeans  implements Serializable {
	private int id;
	private int user_Id;
	private int item_Id;
	private int item_Price;
	private Date buyDate;
	private int Price;
	
	public int getPrice() {
		return Price;
	}
	public void setPrice(int totalPrice) {
		Price = totalPrice;
	}
	public int getItem_Price() {
		return item_Price;
	}
	public void setItem_Price(int item_Price) {
		this.item_Price = item_Price;
	}
	public int getItem_Id() {
		return item_Id;
	}
	public void setItem_Id(int item_id) {
		this.item_Id = item_id;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUser_Id() {
		return user_Id;
	}
	public void setUser_Id(int userId) {
		this.user_Id = userId;
	}
	public Date getBuyDate() {
		return buyDate;
	}
	public void setBuyDate(Date buyDate) {
		this.buyDate = buyDate;
	}

	

}
