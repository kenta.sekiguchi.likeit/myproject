package yproject;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.ItemDataBeans;
import dao.ItemDAO;

/**
 * Servlet implementation class BuyConfirm
 */
@WebServlet("/BuyConfirm")
public class BuyConfirm extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuyConfirm() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session=request.getSession();
		
		int id = Integer.parseInt(request.getParameter("item_id"));

		try {
			ItemDataBeans item = ItemDAO.getItemByItemID(id);
			BuyDataBeans bdb = new BuyDataBeans();
			
			bdb.setUser_Id((int) session.getAttribute("userId"));
			bdb.setItem_Id(item.getId());
			bdb.setPrice(item.getPrice());
			//購入確定で利用
			session.setAttribute("bdb", bdb);
			request.setAttribute("item", item);
			
			request.getRequestDispatcher("/WEB-INF/jsp/buyConfirm.jsp").forward(request, response);
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		
	}
	
}
