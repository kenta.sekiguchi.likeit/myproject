package yproject;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.BuyDetailDataBeans;
import dao.BuyDAO;
import dao.BuyDetailDAO;

/**
 * Servlet implementation class BuyResult
 */
@WebServlet("/BuyResult")
public class BuyResult extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuyResult() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		
		// セッション切ってるだけ
		//BuyDataBeans bdb = (BuyDataBeans) McHelper.cutSessionAttribute(session, "bdb");
		BuyDataBeans bdb =(BuyDataBeans) session.getAttribute("bdb");
		//int userId=(Integer)session.getAttribute("userId");
		
		// セッションからカート情報を取得
		//ArrayList<ItemDataBeans> cart = (ArrayList<ItemDataBeans>) EcHelper.cutSessionAttribute(session, "cart");
		// セッション切ってるだけ
		//BuyDataBeans bdb = (BuyDataBeans) EcHelper.cutSessionAttribute(session, "bdb");

		// 購入情報を登録bddb.setItemId(cartInItem.getId());
		int buyId = 0;
		try {
			buyId = BuyDAO.insertBuy(bdb);
		} catch (SQLException e1) {
			// TODO 自動生成された catch ブロック
			e1.printStackTrace();
		}
		int item_id = Integer.parseInt(request.getParameter("item_id"));
		
		//session.setAttribute("buyId", buyId);
		// 購入詳細情報を購入情報IDに紐づけして登録
			BuyDetailDataBeans bddb = new BuyDetailDataBeans();
			//ユーザーID
			bddb.setBuyId(buyId);
			//アイテムID
			bddb.setItemId(item_id);

			//sqlに2種類のID登録
			try {
				BuyDetailDAO.insertBuyDetail(bddb);
			} catch (SQLException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}
			
		
		//tbuyのデータ
		//BuyDataBeans resultBDB = BuyDAO.getBuyDataBeansByBuyId(buyId);
		//request.setAttribute("resultBDB", resultBDB);
		request.getRequestDispatcher("/WEB-INF/jsp/buyresult.jsp").forward(request, response);
		
	
	}

}
