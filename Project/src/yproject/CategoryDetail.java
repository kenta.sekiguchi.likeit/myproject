package yproject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import dao.ItemDAO;

/**
 * Servlet implementation class CategoryDetail
 */
@WebServlet("/CategoryDetail")
public class CategoryDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public CategoryDetail() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();

		List<ItemDataBeans> categoryList = new ArrayList<ItemDataBeans>();
		List<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();

		// URLからGETパラメータとしてIDを受け取る
		String id = request.getParameter("id");

			ItemDAO idao=new ItemDAO();
			itemList=idao.findByItemInfo(id);
			request.setAttribute("itemList", itemList);
			request.getRequestDispatcher("/WEB-INF/jsp/home.jsp").forward(request, response);
	}
}
