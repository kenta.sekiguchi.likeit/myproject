package yproject;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.ItemDataBeans;
import dao.BuyDAO;
import dao.BuyDetailDAO;

/**
 * 購入履歴画面
 * @author d-yamaguchi
 *
 */
@WebServlet("/UserBuyHistoryDetail")
public class UserBuyHistoryDetail extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		try {
	
		String Id = request.getParameter("Id");
		int i = Integer.parseInt(Id);
		BuyDataBeans bdb=BuyDAO.getBuyDataBeansByBuyId(i);
		
		ArrayList<ItemDataBeans> list=BuyDetailDAO.getItemDataBeansListByBuyId(i);	    
		
		//request.setAttribute("list", list);
		request.setAttribute("bdb",bdb);
		request.setAttribute("list",list);
		
		
		request.getRequestDispatcher("/WEB-INF/jsp/userbuyhistorydetail.jsp").forward(request, response);
	} catch (Exception e) {
		e.printStackTrace();
		session.setAttribute("errorMessage", e.toString());
		response.sendRedirect("Error");
	}

	}

}

