package yproject;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDAO;

/**
 * Servlet implementation class RegistConfirm
 */
@WebServlet("/RegistResult")
public class RegistResult extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegistResult() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/* 文字化け対策 */
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		try {
			String inputNname = request.getParameter("nname");
			String inputMail = request.getParameter("mail");
			String inputPassword = request.getParameter("password");
			String inputName = request.getParameter("name");
			String inputKanaName = request.getParameter("kananame");
			String inputDate = request.getParameter("date");
			
			UserDataBeans udb = new UserDataBeans();
			udb.setNname(inputNname);
			udb.setMail(inputMail);
			udb.setPassword(inputPassword);
			udb.setName(inputName);
			udb.setKananame(inputKanaName);
			udb.setDate(inputDate);
			
			UserDAO.insertUser(udb);
			request.setAttribute("udb", udb);
			request.getRequestDispatcher("/WEB-INF/jsp/registresult.jsp").forward(request, response);
				
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}
}
