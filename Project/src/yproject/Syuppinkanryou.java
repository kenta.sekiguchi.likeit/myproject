package yproject;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import dao.ItemDAO;

/**
 * Servlet implementation class Syuppinkanryou
 */
@WebServlet("/Syuppinkanryou")
public class Syuppinkanryou extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Syuppinkanryou() {
        super();
        // TODO Auto-generated constructor stub
    }
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session=request.getSession();
		int userId=(Integer)session.getAttribute("userId");
		
		String file_name = request.getParameter("pic");
		int category_id = Integer.parseInt(request.getParameter("category_id"));
		int status_id = Integer.parseInt(request.getParameter("status_id"));
		String title = request.getParameter("title");
		String detail = request.getParameter("detail");
		String area = request.getParameter("area");
		String days = request.getParameter("days");
		int price=Integer.parseInt(request.getParameter("price"));
		
		ItemDataBeans idb = new ItemDataBeans();
		idb.setName(title);
		idb.setDetail(detail);
		idb.setFile_name(file_name);
		idb.setPrice(price);
		idb.setCate_id(category_id);
		idb.setArea(area);
		idb.setStatus_id(status_id);
		idb.setDays(days);
		idb.setUser_id(userId);
		
		try {
			ItemDAO.insertItem(idb);
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		
		request.getRequestDispatcher("/WEB-INF/jsp/syuppinkanryou.jsp").forward(request, response);
	}

}
