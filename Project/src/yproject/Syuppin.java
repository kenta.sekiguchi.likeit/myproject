package yproject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.CategoryDataBeans;
import beans.StatusDataBeans;
import dao.CategoryDAO;
import dao.StatusDAO;

/**
 * Servlet implementation class Syuppin
 */
@WebServlet("/Syuppin")
public class Syuppin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Syuppin() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		List<CategoryDataBeans> categoryList = new ArrayList<CategoryDataBeans>();
		CategoryDAO c=new CategoryDAO();
		categoryList=c.getCategory();
		
		List<StatusDataBeans> statusList = new ArrayList<StatusDataBeans>();
		StatusDAO s=new StatusDAO();
		statusList=s.getStatus();
		
		request.setAttribute("categoryList", categoryList);
		request.setAttribute("statusList", statusList);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/syuppin.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
