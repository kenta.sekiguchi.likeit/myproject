package yproject;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.CategoryDataBeans;
import beans.ItemDataBeans;
import dao.CategoryDAO;
import dao.ItemDAO;

/**
 * Servlet implementation class Home
 */
@WebServlet("/Home")
public class Home extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Home() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	List<CategoryDataBeans> categoryList = new ArrayList<CategoryDataBeans>();
	ArrayList<ItemDataBeans> itemList;
	try {
		itemList = ItemDAO.getRandItem(4);
		CategoryDAO dao=new CategoryDAO();
		
		categoryList=dao.getCategory();
		
		//itemList=dao.findByItemInfo();
		request.setAttribute("categoryList", categoryList);
		request.setAttribute("itemList", itemList);
		request.getRequestDispatcher("/WEB-INF/jsp/home.jsp").forward(request, response);
	} catch (SQLException e) {
		// TODO 自動生成された catch ブロック
		e.printStackTrace();
	}

	}

}
