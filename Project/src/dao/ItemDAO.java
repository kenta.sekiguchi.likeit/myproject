package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.ItemDataBeans;

public class ItemDAO {
	
    public List<ItemDataBeans> findByItemInfo() {
    	ArrayList<ItemDataBeans> list = new ArrayList<ItemDataBeans>();

    	Connection conn = null;

    try {
        // データベースへ接続
        conn = DBManager.getConnection();
        // SELECT文を準備
       String sql = "SELECT * FROM m_item";

        // SELECTを実行し、結果表を取得
       Statement stmt = conn.createStatement();
       ResultSet rs = stmt.executeQuery(sql);

        // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
        while (rs.next()) {
        	ItemDataBeans item=new ItemDataBeans();
        // 必要なデータのみインスタンスのフィールドに追加
        	item.setId(rs.getInt("id"));
        	item.setName(rs.getString("name"));
        	item.setDetail(rs.getString("detail"));
        	item.setPrice(rs.getInt("price"));
        	item.setFile_name(rs.getString("file_name"));
        	list.add(item);
        }
    } catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {
        // データベース切断
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
    return list;
}
    
    //引数減らした2号機
    public List<ItemDataBeans> findByItemInfo(String id) {
    	ArrayList<ItemDataBeans> list=new ArrayList<ItemDataBeans>();
    Connection conn = null;
    try {
        // データベースへ接続
        conn = DBManager.getConnection();
        // SELECT文を準備
        String sql = "SELECT * FROM m_item WHERE cate_id = ?";

         // SELECTを実行し、結果表を取得
        PreparedStatement pStmt = conn.prepareStatement(sql);
        pStmt.setString(1, id);
        ResultSet rs = pStmt.executeQuery();

        /*/ 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
        if (!rs.next()) {
            return null;
        }/*/
        while (rs.next()) {
        	ItemDataBeans item=new ItemDataBeans();
        // 必要なデータのみインスタンスのフィールドに追加
        	item.setId(rs.getInt("id"));
        	item.setName(rs.getString("name"));
        	item.setDetail(rs.getString("detail"));
        	item.setPrice(rs.getInt("price"));
        	item.setFile_name(rs.getString("file_name"));
        	item.setCate_id(rs.getInt("cate_id"));
        	list.add(item);
        }

    } catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {
        // データベース切断
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
    return list;
}
	
    //引数減らした2号機
    public List<ItemDataBeans> findByItemInfo(int id) {
    	ArrayList<ItemDataBeans> list=new ArrayList<ItemDataBeans>();
    Connection conn = null;
    try {
        // データベースへ接続
        conn = DBManager.getConnection();
        // SELECT文を準備
        String sql = "SELECT * FROM m_item WHERE user_id = ?";

         // SELECTを実行し、結果表を取得
        PreparedStatement pStmt = conn.prepareStatement(sql);
        pStmt.setInt(1, id);
        ResultSet rs = pStmt.executeQuery();

        /*/ 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
        if (!rs.next()) {
            return null;
        }/*/
        while (rs.next()) {
        	ItemDataBeans item=new ItemDataBeans();
        // 必要なデータのみインスタンスのフィールドに追加
        	item.setId(rs.getInt("id"));
        	item.setName(rs.getString("name"));
        	item.setDetail(rs.getString("detail"));
        	item.setPrice(rs.getInt("price"));
        	item.setFile_name(rs.getString("file_name"));
        	item.setCate_id(rs.getInt("cate_id"));
        	item.setArea(rs.getString("area"));
        	item.setDays(rs.getString("days"));
        	list.add(item);
        }

    } catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {
        // データベース切断
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
    return list;
}
      
	public static ArrayList<ItemDataBeans> getRandItem(int limit) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM m_item ORDER BY RAND() LIMIT ? ");
			st.setInt(1, limit);

			ResultSet rs = st.executeQuery();

			ArrayList<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();

			while (rs.next()) {
				ItemDataBeans item = new ItemDataBeans();
				item.setId(rs.getInt("id"));
				item.setName(rs.getString("name"));
				item.setDetail(rs.getString("detail"));
				item.setPrice(rs.getInt("price"));
				item.setFile_name(rs.getString("file_name"));
				itemList.add(item);
			}
			System.out.println("getAllItem completed");
			return itemList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}
	
	/**
	 * 商品IDによる商品検索
	 * @param itemId
	 * @return ItemDataBeans
	 * @throws SQLException
	 */
	public static ItemDataBeans getItemByItemID(int id) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM m_item WHERE id = ?");
			st.setInt(1, id);

			ResultSet rs = st.executeQuery();

			ItemDataBeans item = new ItemDataBeans();
			if (rs.next()) {
				item.setId(rs.getInt("id"));
				item.setName(rs.getString("name"));
				item.setDetail(rs.getString("detail"));
				item.setPrice(rs.getInt("price"));
				item.setFile_name(rs.getString("file_name"));
			}

			System.out.println("searching item by itemID has been completed");

			return item;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}
	
	public static void insertItem(ItemDataBeans idb) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("INSERT INTO m_item(name,detail,price,file_name,user_id,cate_id,status_id,area,days) VALUES(?,?,?,?,?,?,?,?,?)");
			st.setString(1, idb.getName());
			st.setString(2, idb.getDetail());
			st.setInt(3, idb.getPrice());
			st.setString(4, idb.getFile_name());
			st.setInt(5, idb.getUser_id());
			st.setInt(6, idb.getCate_id());
			st.setInt(7, idb.getStatus_id());
			st.setString(8, idb.getArea());
			st.setString(9, idb.getDays());
			
			st.executeUpdate();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

}
