package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.CategoryDataBeans;

public class CategoryDAO {
	
	public List<CategoryDataBeans> getCategory() {
    	ArrayList<CategoryDataBeans> list = new ArrayList<CategoryDataBeans>();

    	Connection conn = null;

    try {
        // データベースへ接続
        conn = DBManager.getConnection();
        // SELECT文を準備
       String sql = "SELECT * FROM category";

        // SELECTを実行し、結果表を取得
       Statement stmt = conn.createStatement();
       ResultSet rs = stmt.executeQuery(sql);

        // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
        while (rs.next()) {
        	CategoryDataBeans cate=new CategoryDataBeans();
        // 必要なデータのみインスタンスのフィールドに追加
        	cate.setId(rs.getInt("id"));
        	cate.setName(rs.getString("name"));
        	list.add(cate);
        }
    } catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {
        // データベース切断
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
    return list;
}

}