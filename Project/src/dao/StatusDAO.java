package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.StatusDataBeans;

public class StatusDAO {
	public List<StatusDataBeans> getStatus() {
    	ArrayList<StatusDataBeans> list = new ArrayList<StatusDataBeans>();

    	Connection conn = null;

    try {
        // データベースへ接続
        conn = DBManager.getConnection();
        // SELECT文を準備
       String sql = "SELECT * FROM status";

        // SELECTを実行し、結果表を取得
       Statement stmt = conn.createStatement();
       ResultSet rs = stmt.executeQuery(sql);

        // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
        while (rs.next()) {
        	StatusDataBeans status=new StatusDataBeans();
        // 必要なデータのみインスタンスのフィールドに追加
        	status.setId(rs.getInt("id"));
        	status.setName(rs.getString("name"));
        	list.add(status);
        }
    } catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {
        // データベース切断
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
    return list;
}

}
