package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.ItemDataBeans;
import beans.UserDataBeans;
import yproject.McHelper;

public class UserDAO {

	public static void insertUser(UserDataBeans udb) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("INSERT INTO t_user(name,kananame,n_name,mail,login_password,create_date) VALUES(?,?,?,?,?,?)");
			st.setString(1, udb.getName());
			st.setString(2, udb.getKananame());
			st.setString(3, udb.getNname());
			st.setString(4, udb.getMail());
			st.setString(5, McHelper.getSha256(udb.getPassword()));
			st.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			st.executeUpdate();
			System.out.println("inserting user has been completed");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

    /*public List<CategoryDataBeans> findBycategoryInfo() {
    	ArrayList<CategoryDataBeans> list = new ArrayList<CategoryDataBeans>();

    	Connection conn = null;

    try {
        // データベースへ接続
        conn = DBManager.getConnection();
        // SELECT文を準備
       String sql = "SELECT * FROM category";

        // SELECTを実行し、結果表を取得
       Statement stmt = conn.createStatement();
       ResultSet rs = stmt.executeQuery(sql);

        // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
        while (rs.next()) {
        	CategoryDataBeans category=new CategoryDataBeans();
        // 必要なデータのみインスタンスのフィールドに追加
        	category.setId(rs.getInt("id"));
        	category.setName(rs.getString("name"));

        	list.add(category);
        }

    } catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {
        // データベース切断
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
    return list;
}
*/


  //引数減らした2号機
    public List findByitemInfo(String id) {
    	ArrayList<ItemDataBeans> list=new ArrayList<ItemDataBeans>();
    Connection conn = null;
    try {
        // データベースへ接続
        conn = DBManager.getConnection();
        // SELECT文を準備
        String sql = "SELECT * FROM m_item JOIN category"+"on m_item.cate_id=category.id"+" WHERE m_item.id = ?";

         // SELECTを実行し、結果表を取得
        PreparedStatement pStmt = conn.prepareStatement(sql);
        pStmt.setString(1, id);
        ResultSet rs = pStmt.executeQuery();

        /*/ 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
        if (!rs.next()) {
            return null;
        }/*/
        while (rs.next()) {
        	ItemDataBeans item=new ItemDataBeans();
        // 必要なデータのみインスタンスのフィールドに追加
        	item.setId(rs.getInt("id"));
        	item.setName(rs.getString("name"));
        	item.setDetail(rs.getString("detail"));
        	item.setPrice(rs.getInt("price"));
        	item.setFile_name(rs.getString("file_name"));
        	item.setCate_id(rs.getInt("cate_id"));
        	list.add(item);
        }


    } catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {
        // データベース切断
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
    return list;
}

	public static int getUserId(String mail, String password) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM t_user WHERE mail = ?");
			st.setString(1, mail);

			ResultSet rs = st.executeQuery();

			int userId = 0;
			while (rs.next()) {
				if (McHelper.getSha256(password).equals(rs.getString("login_password"))) {
					userId = rs.getInt("id");
					System.out.println("login succeeded");
					break;
				}
			}

			System.out.println("searching userId by loginId has been completed");
			return userId;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}
}
