<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="UTF-8">
	<title>title</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="home.css">
</head>
 <body>
    <!-- ナビゲーションバー -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light height">
      <a class="navbar-brand" href="Home">メルカリ（仮）</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <button class="btn btn-danger my-2 my-sm-0" type="submit" onclick="location.href='Regist'">新規会員登録</button>　
        <button class="btn btn-primary my-2 my-sm-0" type="submit" onclick="location.href='Login'">ログイン</button>　
        <form class="form-inline my-2 my-lg-0">
          <input class="form-control mr-sm-2" type="search" placeholder="何かお探しですか" aria-label="Search">
          <button class="btn btn-success my-2 my-sm-0" type="submit">検索</button>　
        </form>
      </div>
    </nav>
    <!-- ナビゲーションバー　-->
    
    <!-- カード　-->
    <form action="BuyConfirm" method="POST">
    <input type="hidden" name="item_id" value="${item.id}">
	    <div class="row">
	        <div class="col"></div>
	            <div class="col-sm-6">
	                <div class="card" style="max-width:70rem; min-height:50rem;">
	                    <div class="cardbody">
	                        
	                        <div class="text-center"><h2>${item.name}</h2></div>
	                        <a>『ZARA セーター』は、まなみさんから出品されました。ザラ（ニット/セーター/レディース）の商品で、福岡県から1~2日で発送されます。</a><br>
	                        <img src="img/${item.file_name}"class="img-fluid" alt="レスポンシブ画像">
	                        <table class="table">
	                          <thead>
	                            <tr>
	                                <th scope="col">出品者</th>
	                                <th scope="col">カテゴリー</th>
	                                <th scope="col">ブランド</th>
	                                <th scope="col">商品のサイズ</th>
	                                <th scope="col">商品の状態</th>
	                            </tr>
	                             </thead>
	                              <tbody>
	                            <tr>
	                                <td>まなみ</td>
	                                <td>レディーストップスニット/セーター</td>
	                                <td>ザラ</td>
	                                <td>M</td>
	                                <td>目立った傷や汚れなし</td>
	                            </tr>
	                          </tbody>
	                              <tr>                                
	                                <th scope="col">配送料の負担</th>
	                                <th scope="col">配送の方法</th>
	                                <th scope="col">配送元地域</th>
	                                <th scope="col">発送日の目安</th>
	                            </tr>                         
	                          <tbody>
	                              <tr>                                
	                                <td>送料込み(出品者負担)</td>
	                                <td>らくらくメルカリ便</td>
	                                <td>福岡県</td>
	                                <td>1~2日で発送</td>
	                            </tr>
	                          </tbody>
	                        </table>
	                        <h1 class="text-center">￥${item.price}</h1>
	                        <button type="submit" class="btn btn-danger btn-lg btn-block">購入画面に進む</button>
	                        <a>${item.detail}</a>
	                    </div>
	                </div>
	            </div>
	        <div class="col" ></div>
	    </div>
    </form>    
    <!-- カード　-->
    <br><br>
    </body>
</html>