<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
 integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
    <body>
        <div class="row">
            <div class="col"></div>
            <div class="col"><h1>新規登録画面</h1></div>
            <div class="col"></div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-4"></div>
                <div class="col-sm-4">アカウントをお持ちでない方はこちら</div>
                <div class="col-sm-4"></div>
                <div class="col"></div>
                <div class="col"><button type="button" onclick="location.href='Regist'" class="btn btn-primary btn-lg">新規会員登録</button></div>
                <div class="col"></div>
            </div>
            <hr color="#999999">
            <div class="row">
                <div>
                購入に進むにはログインが必要です。<br>
                アカウントをお持ちでない場合は上記の「新規会員登録」より会員登録をしてください。
                </div>
            </div>
                <br><br>
        <form action="LoginResult" method="POST">
          <div class="form-group">
            <label for="exampleInputEmail1"></label>
            <input type="email" name="mail" class="form-control" id="exampleInputEmail1" placeholder="メールアドレス">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1"></label>
            <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="パスワード">
          </div>
            <div class="row">
                <div class="col"></div>
                <div class="col"><button type="submit" class="btn btn-danger">ログイン</button></div>
                <div class="col"></div>
            </div>
        </form>
          <br>
        <a href="#">パスワードをお忘れの方</a>
    </div>
  </body>
</html>