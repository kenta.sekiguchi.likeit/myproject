<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="UTF-8">
	<title>title</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="home.css">
</head>
 <body> 
        <div class="alert alert-success" role="alert">
            <h4 class="alert-heading"><a href="Home">←</a>　出品完了</h4>
        </div>
        <div class="container">
                    <!-- カード　-->
                <div class="card" style="min-height:50rem;">
                    <div class="cardbody">
                        <br><br><br><br><br><br><br><br><br><br><br><br><br>
                        <div class="text-center">
                            <h4>出品が完了しました！</h4>
                        </div>
                    </div>
                </div>   
        <!-- カード　-->
        </div>
    </body>
</html>