<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="UTF-8">
	<title>title</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="home.css">
</head>
<body>
	<div class="alert alert-success" role="alert">
            <div class="row">
                <div><h4>ホーム</h4></div>
                <div class="col">
                    <div class="float-right"><a href="Mypage">マイページへ</a></div>
                </div>
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="なにをお探しですか">
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-primary">検索</button>
                    </span>
                </div>
            </div>
    	</div>
    <div class="container">
    	<ul class="nav nav-tabs">
	        <c:forEach var="category" items="${categoryList}">
	            <li class="nav-item"><a class="nav-link active" href="CategoryDetail?id=${category.id}">${category.name}</a></li>
 			</c:forEach>
	    </ul>
	        <div class="row">
	        	<c:forEach var="item" items="${itemList}">
					<div class="col-md-3">
						<div class="card">
							<div class="card-image">
								<a href="ItemDetail?id=${item.id}"><img src="img/${item.file_name}" class="img-thumbnail"></a>
							</div>
							<div class="card-content">
								<span class="card-title">${item.name}</span>
								<p>${item.price}円
								</p>
							</div>
						</div>
					</div>
				</c:forEach>
	        </div>
    </div>
</body>