<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="UTF-8">
	<title>title</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="home.css">
</head>
    <body>
    <form action="Syuppinkanryou" method="post">
        <div class="alert alert-success" role="alert">
            <h4 class="alert-heading"><a href="Home">←</a>　商品の情報を入力</h4>
        </div>
        <div class="container">
        <br>
       		 画像ファイル<input type="file" name="pic">
            
        <br>
            <a class="text-success">商品の詳細</a>
        <!--下段-->
        <!-- カード　-->
                <div class="card" style="max-height:15rem;">
                    <div class="cardbody">
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">カテゴリー</label>
                                <select class="form-control" name="category_id" id="exampleFormControlSelect1" required>
                                  <c:forEach var="category" items="${categoryList}">
                                  	<option value="${category.id}">${category.name}</option>
                                  </c:forEach>
                                </select>
                        </div>                        
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">商品の状態</label>
                                <select class="form-control" name="status_id" id="exampleFormControlSelect1"　required>
                                  <c:forEach var="status" items="${statusList}">
                                  	<option value="${status.id}">${status.name}</option>
                                  </c:forEach>
                                </select>
                        </div>
                    </div>
                </div>
        <!-- カード　-->&#13;&#10;
        <!--下段-->
            <br>
            <a class="text-success">商品名と説明</a>
        <!--下段-->
        <!-- カード　-->
                <div class="card" style="max-height:15rem;">
                    <div class="cardbody">
                    　<div class="form-group">
                        <label for="exampleFormControlTextarea1">商品名</label>
                        <textarea class="form-control" name="title" id="exampleFormControlTextarea1" rows="1" placeholder="(必須)" required></textarea>
                      </div>
                      <div class="form-group">
                        <label for="exampleFormControlTextarea1">商品の説明</label>
                        <textarea class="form-control" name="detail" id="exampleFormControlTextarea1" rows="6" placeholder="(任意)&#13;&#10;(色、素材、重さ、定価、注意点など)&#13;&#10;&#13;&#10;例:2010年ごろに1万円で購入したジャケットです。ライトグレーで傷はありません。あわせやすいのでおすすめです。&#13;&#10;&#13;&#10;#ジャケット#ジャケットコーデ"></textarea>
                      </div>
                    </div>
                </div>
        <!-- カード　-->
        <!--下段-->
        <br><br><br><br>
            <a class="text-success">配送について</a>
        <!-- カード　-->
                <div class="card" style="max-height:15rem;">
                    <div class="cardbody">
                        <div class="row">
                            <div class="col">
                                <h4>発送元の地域</h4>
                            </div>
                            <div class="col">
                                <select class="form-control" name="area" id="exampleFormControlSelect1"　required>
                                  <option value="北海道">北海道</option>
                                  <option value="東北">東北</option>
                                  <option value="関東">関東</option>
                                  <option value="中部">中部</option>
                                  <option value="近畿">近畿</option>
                                  <option value="中国">中国</option>
                                  <option value="四国">四国</option>
                                  <option value="九州沖縄">九州沖縄</option>
                                </select>
                            </div>
                        </div>
                            <br>
                            <div class="row">
                            <div class="col">
                                <h4>発送までの日数</h4>
                            </div>
                            <div class="col">
                                <select class="form-control" name="days" id="exampleFormControlSelect1"　required>
                                  <option value="1~2日で発送">1~2日で発送</option>
                                  <option value="2~3日で発送">2~3日で発送</option>
                                  <option value="4~7日で発送">4~7日で発送</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
            <a class="text-success">販売価格(300~9,999,999)</a>
        <!--下段-->
        <!-- カード　-->
                <div class="card" style="max-height:15rem;">
                    <div class="cardbody">
                        <div class="row">
                            <div class="col"><input type="number" name="price" min="300" max="9999999" value="0"><a>円</a></div>
                        </div>
                        <div class="row">
                            <div class="col"><a>販売手数料</a></div>
                            <div class="col"><a>500円</a></div>
                        </div>
                    </div>
                </div>
        <!-- カード　-->
        <!--下段-->
            </div>
            <br>
            <div class="row">
                <div class="mx-auto">
                    <button type="submit" class="btn btn-danger btn-center">出品する</button>
                </div>
            </div>
        <br><br>
        </form>
     </body>
</html>