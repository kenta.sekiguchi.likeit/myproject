<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
 integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
        <div class="container">
        <br>
        <div class="row">
            <div class="col"></div>
            <div class="col"><h1>メルカリ(仮)</h1></div>
            <div class="col"></div>
        </div>
        <br><br>
        <!-- カード　-->
            <div class="card" style="min-height:15rem;">
                <div class="cardbody">
                    <h2 class="text-center">会員情報入力</h2>
                    <br>
                    <br>
                    <hr color="#999999">
                    <form action="RegistResult" method="POST">
                      <div class="form-group">
                        <label for="exampleInputEmail1">ニックネーム</label>
                        <input value="${udb.nname}" name="nname" type="text" placeholder="例)メルカリ(仮)太郎" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">メールアドレス</label>
                        <input value="${udb.mail}" name="mail" type="email" placeholder="pc・携帯どちらでも可" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">パスワード</label>
                        <input name="password" type="password" placeholder="半角英数字" class="form-control" id="exampleInputPassword1">
                      </div>
                        <br>
                        <a>本人確認安心・安全にご利用いただくために、お客さまの本人情報の登録にご協力ください。他のお客さまに公開されることはありません。</a>
                        <br><br>
                        <div class="row">
                            <div class="col">
                                <label for="exampleInputEmail1">お名前(全角) </label>
                                <input name="name" type="text" class="form-control" placeholder="例)田中太郎">
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col">
                                <label for="exampleInputEmail1">お名前カナ(全角) </label>
                                <input name="kananame" type="text" class="form-control" placeholder="例)タナカタロウ">
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="mx-auto">
                                <label for="exampleInputEmail1">生年月日</label><br>
                                <input name="date" type="date" id="3" name="datestart">
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="mx-auto">
                                <button type="submit" class="btn btn-primary">登録完了</button>
                            </div>
                        </div>
                    </form>
                <br><br>
            </div>
        </div>   
    <!-- カード　-->
        <br><br>
    </div>
</body>
</html>